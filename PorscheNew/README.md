# Porsche 992 GT3 R/CUP Dashboard

With the upcoming Porsche 992 GT3 car announced and probably releasing for ACC in the future, I thought this was a perfect oportunity to make a dashboard for the 992.

The dash currently has 5 pages plus one transparent page for VR users to see.

1. RACE1: Contains temps and laptimes
2. RACE2: Fuel (with fuel delta) and laptimes
3. QUALI: A simplified laptime setup, with just current time and delta. Includes a diff meter as well.
4. B/T: Replaces the real-life warmup dash, has full temperature and wear values that are available for brake pads, discs and tyres.
5. DATA: Stint info, damage info, as well as basic telemetry.
6. MAP: A simple static map.

The top light bar is currently only working for a few cars, if it's not working for a car it won't even light up at all. It intends to mimic the in-dash lights, so the 991.2 flashes at 8700 rpm, not 9000 rpm. Some lights are duplicated to fill out the 16 lights.

The top two lights on the left and right indicate tyre slip on the front and rear. They will start green, and turn red if the slip begins to reach the limit.

The bottom lights are different on each side. On the left is the low-fuel light, which will light up red with <2 laps remaining. The right is the invalid lap light. If you go out of the circuit on a practice or quali lap, it will instantly light up red. The indicators are the 5th light on both.

The tyre widget in the bottom has both tyre temp and pressure. Brake bias is +/- from 50%, and a pop-up will appear when it's changed.

![Main](PorscheNew.djson.png)
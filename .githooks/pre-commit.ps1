[CmdletBinding()]
param ()

$badFile = $false

$files = Get-Childitem -Recurse -Filter *.djson -Exclude *_b*.djson
$nFiles = ($files | Measure-Object).Count
write-verbose "Running against $nfiles djson files"
$completed = 0

foreach ($djson in $files) {
  $percent = ($completed / $nFiles * 100)
  Write-Verbose $percent
  Write-Progress -Activity pre-commit -current "Checking $djson" -PercentComplete $percent
  if ((Get-Content $djson | Measure-Object -Line).Lines -le 1) {
    Write-Host "==> File ${djson} hasn't been reformatted."
    $badFile = $true
  }
  $completed += 1
}

Write-Progress -Activity pre-commit -current "Completed." -Completed

if ($badFile) {
  Throw "There are bad files. Please properly format your code."
} else {
  Write-Host "==> Files properly formatted."
}
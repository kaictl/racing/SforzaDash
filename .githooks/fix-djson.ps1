[CmdletBinding()]
param (
    [Parameter()]
    [String]
    $path
)

if (Test-Path -Path $path -PathType Container) {
  Get-ChildItem -Filter *.djson $path `
    | ForEach-Object {
      Write-Host "Fixing $_"
      $js = (Get-Content $_.FullName | ConvertFrom-Json)
      ConvertTo-Json -Depth 50 $js | Out-File $_.FullName
    }
}
/*
 * A list of data for each car.
 */
function ACCCars(CarID) {
    // colors to be replaced with normalized colors
    let red = 'red'
    let tred = 'tomato'
    let green = 'lime'
    let tgreen = 'green'
    let blue = 'blue'
    let lightBlue = 'dodgerBlue'
    let yellow = 'yellow'
    let tyellow = 'gold'
    let orange = 'orange'
    let pink = 'pink'
    let off = '#FF141414'
    let flash = 'flash'
    let blankLight = {color: off, rpm: 0}

    let carList = {
    // GT3
        // Front
        'amr_v12_vantage_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 11, TC2: false, MAP: 8},
            bias: [57, 78],
            tachDelims: [[6100, tgreen], [6500, tyellow], [6900, tred], [7300, blue], [7600, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6100},
                {color: green, rpm: 6300},
                {color: yellow, rpm: 6500},
                {color: yellow, rpm: 6700},
                {color: red, rpm: 6900},
                {color: red, rpm: 7100},
                {color: lightBlue, rpm: 7300},
                {color: lightBlue, rpm: 7500},
            ],
            lightStyle: {
                flash: [{rpm: 7600, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: ['color', 'color']}
            },
            brand: 'Aston Martin'},

        'amr_v8_vantage_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: 8, MAP: 8},
            bias: [57, 78],
            tachDelims: [[6200, tgreen], [6400, tred], [6800, blue], [7000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6200},
                {color: red, rpm: 6400},
                {color: red, rpm: 6600},
                {color: red, rpm: 6600},
                {color: red, rpm: 6600},
                {color: lightBlue, rpm: 6800},
                {color: lightBlue, rpm: 6800},
                {color: lightBlue, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7000, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: ['color', off]}
            },
            brand: 'Aston Martin'},

        'bentley_continental_gt3_2016':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [57, 78],
            tachDelims: [[5900, tgreen], [6900, blue], [7100, tred], [7101, flash]],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5900},
                {color: green, rpm: 6200},
                {color: green, rpm: 6500},
                {color: green, rpm: 6700},
                {color: lightBlue, rpm: 6900},
                {color: lightBlue, rpm: 6900},
                {color: red, rpm: 7100},
                {color: red, rpm: 7100},
            ],
            lightStyle: {
                flash: [{rpm: 7100, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Bentley'},

        'bentley_continental_gt3_2018':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [57, 78],
            tachDelims: [[6300, tgreen], [6500, tyellow], [6900, tred], [6901, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6300},
                {color: yellow, rpm: 6500},
                {color: yellow, rpm: 6700},
                {color: red, rpm: 6900},
            ],
            lightStyle: {
                flash: [{rpm: 6900, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Bentley'},

        'bmw_m4_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 10, TC: 10, TC2: false, MAP: 8},
            bias: [48.5, 68],
            tachDelims: [[5600, tgreen], [6200, tyellow], [6600, tred], [6601, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 5600},
                {color: green, rpm: 6000},
                blankLight,
                {color: yellow, rpm: 6200},
                {color: yellow, rpm: 6400},
                {color: red, rpm: 6600},
            ],
            lightStyle: {
                flash: [{rpm: 6600, style: 'flash', color: [red, off]}],
                pit: {style: '1100000011', color: [pink, off]}
            },
            brand: 'BMW'},

        'bmw_m6_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 5, TC: 10, TC2: false, MAP: 8},
            bias: [47.5, 67.0],
            tachDelims: [[4700, tgreen], [5700, tyellow], [6100, tred], [6350, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 4700},
                {color: green, rpm: 5000},
                {color: green, rpm: 5300},
                {color: green, rpm: 5500},
                {color: yellow, rpm: 5700},
                {color: yellow, rpm: 5900},
                {color: red, rpm: 6100},
                {color: red, rpm: 6250},
            ],
            lightStyle: {
                flash: [{rpm: 6350, style: 'flash', color: ['color', off]}],
                pit: {style: '00000000', color: [off, off]}
            },
            brand: 'BMW'},

        'jaguar_g3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 10, TC2: false, MAP: 6},
            bias: [57, 78],
            tachDelims: [[6800, tgreen], [7400, tyellow], [7700, orange], [8000, tred], [8100, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6800},
                {color: green, rpm: 6800},
                {color: yellow, rpm: 7100},
                {color: yellow, rpm: 7100},
                {color: orange, rpm: 7400},
                {color: orange, rpm: 7400},
                {color: red, rpm: 8000},
                {color: red, rpm: 8000},
            ],
            lightStyle: {
                flash: [{rpm: 8100, style: 'flash', color: ['color', off]}],
                pit: {style: '22222222', color: ['color', 'color']}
            },
            brand: 'Jaguar'},

        'lexus_rc_f_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 5},
            bias: [50, 71],
            tachDelims: [[6000, tgreen], [6600, tyellow], [6800, tred], [7000, blue], [7250, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6000},
                {color: green, rpm: 6200},
                {color: green, rpm: 6400},
                {color: yellow, rpm: 6600},
                {color: red, rpm: 6800},
                {color: red, rpm: 6900},
                {color: lightBlue, rpm: 7000},
                {color: lightBlue, rpm: 7100},
            ],
            lightStyle: {
                flash: [{rpm: 7150, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Lexus'},

        'mercedes_amg_gt3':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 10, TC: 11, TC2: false, MAP: 3},
            bias: [50, 71],
            tachDelims: [[6000, tgreen], [6600, tyellow], [7000, tred], [7100, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6000},
                {color: green, rpm: 6300},
                blankLight,
                {color: yellow, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7100, style: 'flash', color: [red, off]}],
                pit: {style: 'merc', color: ['merc', 'merc']}
            },
            brand: 'Mercedes-Benz'},

        'mercedes_amg_gt3_evo':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 10, TC: 11, TC2: false, MAP: 3},
            bias: [50, 71],
            tachDelims: [[6000, tgreen], [6600, tyellow], [7000, tred], [7100, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6000},
                {color: green, rpm: 6300},
                blankLight,
                {color: yellow, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7100, style: 'flash', color: [red, off]}],
                pit: {style: 'merc', color: ['merc', 'merc']}
            },
            brand: 'Mercedes-Benz'},

        'nissan_gt_r_gt3_2017':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 7, TC2: false, MAP: 4},
            bias: [47.5, 67],
            tachDelims: [[6500, tgreen], [6800, tyellow], [7000, blue], [7200, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: off, rpm: 0},
                {color: lightBlue, rpm: 6500},
                {color: lightBlue, rpm: 6800},
                {color: lightBlue, rpm: 7000},
                {color: red, rpm: 7200},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: [red, off]}],
                pit: {style: '1000000001', color: [blue, off]}
            },
            brand: 'Nissan'},

        'nissan_gt_r_gt3_2018':
            {engine: 'FR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 9, TC2: false, MAP: 4},
            bias: [47.5, 67],
            tachDelims: [[6500, tgreen], [6800, tyellow], [7000, blue], [7200, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: off, rpm: 0},
                {color: lightBlue, rpm: 6500},
                {color: lightBlue, rpm: 6800},
                {color: lightBlue, rpm: 7000},
                {color: red, rpm: 7200},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: [red, off]}],
                pit: {style: '1000000001', color: [blue, yellow]}
            },
            brand: 'Nissan'},

        // Mid,
        'audi_r8_lms':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [50, 71],
            tachDelims: [[5700, tgreen], [6800, tyellow], [7200, tred], [8000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 6000},
                blankLight,
                {color: green, rpm: 6300},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
                {color: red, rpm: 7400},
                blankLight,
                {color: red, rpm: 7600},
                {color: red, rpm: 7800},
            ],
            lightStyle: {
                flash: [{rpm: 8000, style: 'flash', color: [blue, off]}],
                pit: {style: '1010110101', color: [blue, off]}
            },
            brand: 'Audi'},

        'audi_r8_lms_evo':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [50, 71],
            tachDelims: [[5700, tgreen], [6800, tyellow], [7200, tred], [8000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 6000},
                blankLight,
                {color: green, rpm: 6300},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
                {color: red, rpm: 7400},
                blankLight,
                {color: red, rpm: 7600},
                {color: red, rpm: 7800},
            ],
            lightStyle: {
                flash: [{rpm: 8000, style: 'flash', color: [blue, off]}],
                pit: {style: '1010110101', color: [blue, off]}
            },
            brand: 'Audi'},

        'audi_r8_lms_evo_ii':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: 8, MAP: 8},
            bias: [50, 71],
            tachDelims: [[5700, tgreen], [6800, tyellow], [7200, tred], [8000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 6000},
                blankLight,
                {color: green, rpm: 6300},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
                {color: red, rpm: 7400},
                blankLight,
                {color: red, rpm: 7600},
                {color: red, rpm: 7800},
            ],
            lightStyle: {
                flash: [{rpm: 8000, style: 'flash', color: [blue, off]}],
                pit: {style: '1010110101', color: [blue, off]}
            },
            brand: 'Audi'},

        'ferrari_488_gt3':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 11, TC2: 11, MAP: 12},
            bias: [47, 68],
            tachDelims: [[6100, tgreen], [6900, tyellow], [7000, tred], [7250, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6100},
                {color: green, rpm: 6300},
                {color: green, rpm: 6500},
                {color: green, rpm: 6700},
                {color: yellow, rpm: 6900},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7250, style: 'flash', color: [red, off]}],
                pit: {style: '111111', color: [blue, off]}
            },
            brand: 'Ferrari'},

        'ferrari_488_gt3_evo':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 11, TC2: 11, MAP: 12},
            bias: [47, 68],
            tachDelims: [[6100, tgreen], [6900, tyellow], [7000, tred], [7250, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6100},
                {color: green, rpm: 6300},
                {color: green, rpm: 6500},
                {color: green, rpm: 6700},
                {color: yellow, rpm: 6900},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7250, style: 'flash', color: [red, off]}],
                pit: {style: '111111', color: [blue, off]}
            },
            brand: 'Ferrari'},

        'honda_nsx_gt3':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: 8, MAP: 8},
            bias: [50, 71],
            tachDelims: [[6200, tgreen], [6700, tyellow], [6900, orange], [7100, tred], [7200, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6200},
                {color: green, rpm: 6200},
                {color: green, rpm: 6500},
                {color: green, rpm: 6500},
                {color: yellow, rpm: 6700},
                {color: yellow, rpm: 6700},
                {color: orange, rpm: 6900},
                {color: orange, rpm: 6900},
                {color: red, rpm: 7100},
                {color: red, rpm: 7100},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: ['color', off]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'Honda'},

        'honda_nsx_gt3_evo':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: 8, MAP: 8},
            bias: [50, 71],
            tachDelims: [[6200, tgreen], [6700, tyellow], [6900, orange], [7100, tred], [7200, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6200},
                {color: green, rpm: 6200},
                {color: green, rpm: 6500},
                {color: green, rpm: 6500},
                {color: yellow, rpm: 6700},
                {color: yellow, rpm: 6700},
                {color: orange, rpm: 6900},
                {color: orange, rpm: 6900},
                {color: red, rpm: 7100},
                {color: red, rpm: 7100},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: ['color', off]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'Honda'},

        'lamborghini_gallardo_rex':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 4},
            bias: [50, 71],
            tachDelims: [[7300, tgreen], [7600, tyellow], [7900, tred], [8200, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 7300},
                {color: green, rpm: 7300},
                {color: yellow, rpm: 7600},
                {color: yellow, rpm: 7600},
                {color: red, rpm: 7900},
                {color: red, rpm: 7900},
                {color: red, rpm: 8200},
                {color: red, rpm: 8200},
            ],
            lightStyle: {
                flash: [{rpm: 8200, style: 'none', color: ['color', 'color']}],
                pit: {style: '00000000', color: [off, off]}
            },
            brand: 'Lamborghini'},

        'lamborghini_huracan_gt3':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [50, 71],
            tachDelims: [[5700, tgreen], [6800, tyellow], [7200, tred], [8000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 6000},
                {color: green, rpm: 6300},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
                {color: red, rpm: 7400},
                {color: red, rpm: 7600},
                {color: red, rpm: 7800},
            ],
            lightStyle: {
                flash: [{rpm: 8000, style: 'flash', color: [blue, off]}],
                pit: {style: '1111100000', color: [blue, off]}
            },
            brand: 'Lamborghini'},

        'lamborghini_huracan_gt3_evo':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 8},
            bias: [50, 71],
            tachDelims: [[5700, tgreen], [6800, tyellow], [7200, tred], [8000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 6000},
                {color: green, rpm: 6300},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
                {color: red, rpm: 7400},
                {color: red, rpm: 7600},
                {color: red, rpm: 7800},
            ],
            lightStyle: {
                flash: [{rpm: 8000, style: 'flash', color: [blue, off]}],
                pit: {style: '0011111100', color: [blue, off]}
            },
            brand: 'Lamborghini'},

        'mclaren_650s_gt3':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: 9},
            bias: [47, 68],
            tachDelims: [[6100, tgreen], [6600, tred], [7000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6100},
                {color: green, rpm: 6250},
                {color: green, rpm: 6400},
                {color: green, rpm: 6500},
                {color: red, rpm: 6600},
                {color: red, rpm: 6700},
                {color: red, rpm: 6800},
                {color: red, rpm: 6900},
                {color: lightBlue, rpm: 7000},
                {color: lightBlue, rpm: 7000},
                {color: lightBlue, rpm: 7000},
                {color: lightBlue, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7000, style: 'none', color: ['color', 'color']}],
                pit: {style: '122222222222', color: [green, green]}
            },
            brand: 'McLaren'},

        'mclaren_720s_gt3':
            {engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: 12, TC: 12, TC2: false, MAP: 13},
            bias: [47, 68],
            tachDelims: [[5900, tgreen], [6500, tyellow], [6700, orange], [6900, tred], [7100, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5900},
                {color: green, rpm: 6100},
                blankLight,
                {color: green, rpm: 6300},
                {color: green, rpm: 6400},
                {color: yellow, rpm: 6500},
                {color: yellow, rpm: 6600},
                {color: orange, rpm: 6700},
                {color: orange, rpm: 6800},
                blankLight,
                {color: red, rpm: 6900},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7100, style: 'flash', color: [blue, off]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'McLaren'},

        // Rear,
        'porsche_991_gt3_r':
            {engine: 'RR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 11, TC2: false, MAP: 10},
            bias: [43, 64],
            tachDelims: [[7400, tgreen], [8000, tyellow], [8600, tred], [9000, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 7400},
                {color: green, rpm: 7600},
                {color: green, rpm: 7800},
                {color: yellow, rpm: 8000},
                {color: yellow, rpm: 8200},
                {color: yellow, rpm: 8400},
                {color: red, rpm: 8600},
                {color: red, rpm: 8800},
            ],
            lightStyle: {
                flash: [{rpm: 9000, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Porsche'},

        'porsche_991ii_gt3_r':
            {engine: 'RR',
            carClass: 'GT3',
            ECU: {ABS: 11, TC: 11, TC2: 11, MAP: 10},
            bias: [43, 64],
            tachDelims: [[7400, tgreen], [8000, tyellow], [8500, tred], [8700, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 7400},
                {color: green, rpm: 7700},
                {color: yellow, rpm: 8000},
                {color: yellow, rpm: 8300},
                {color: red, rpm: 8500},
            ],
            lightStyle: {
                flash: [{rpm: 8700, style: 'flash', color: [blue, off]}],
                pit: {style: '1111111111', color: [blue, off]}
            },
            brand: 'Porsche'},

    // Cup
        'lamborghini_huracan_st':
            {engine: 'FR',
            carClass: 'CUP',
            ECU: {ABS: 7, TC: 7, TC2: false, MAP: false},
            bias: [50, 71],
            tachDelims: [[6200, tgreen], [6800, blue], [7100, tyellow], [7400, tred], [7900, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6200},
                {color: lightBlue, rpm: 6800},
                {color: yellow, rpm: 7100},
                {color: red, rpm: 7400},
            ],
            lightStyle: {
                flash: [{rpm: 7900, style: 'flash', color: ['color', off]}],
                pit: {style: '1111111111', color: [blue, off]}
            },
            brand: 'Lamborghini'},
        'lamborghini_huracan_st_evo2':
            {engine: 'FR',
            carClass: 'CUP',
            ECU: {ABS: 11, TC: 8, TC2: false, MAP: false},
            bias: [50, 71],
            tachDelims: [[7600, tgreen], [7800, blue], [8000, tyellow], [8200, tred], [8400, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 7600},
                {color: lightBlue, rpm: 7800},
                {color: yellow, rpm: 8000},
                {color: red, rpm: 8200},
            ],
            lightStyle: {
                flash: [{rpm: 8400, style: 'flash', color: ['color', off]}],
                pit: {style: '1111111111', color: [blue, off]}
            },
            brand: 'Lamborghini'},
        'porsche_991ii_gt3_cup':
            {engine: 'RR',
            carClass: 'CUP',
            ECU: {ABS: 11, TC: false, TC2: false, MAP: false},
            bias: [50, 65],
            tachDelims: [[7000, tgreen], [7600, tyellow], [8000, tred], [8300, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 7000},
                {color: green, rpm: 7200},
                {color: green, rpm: 7400},
                {color: yellow, rpm: 7600},
                {color: yellow, rpm: 7800},
                {color: yellow, rpm: 7900},
                {color: red, rpm: 8000},
                {color: red, rpm: 8100},
            ],
            lightStyle: {
                flash: [{rpm: 8300, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Porsche'},
        'porsche_992_gt3_cup':
            {engine: 'RR',
            carClass: 'CUP',
            ECU: {ABS: 11, TC: false, TC2: false, MAP: false},
            bias: [50, 65],
            tachDelims: [[7000, tgreen], [7600, tyellow], [8050, tred], [8300, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 7000},
                {color: green, rpm: 7200},
                {color: green, rpm: 7400},
                {color: yellow, rpm: 7600},
                {color: yellow, rpm: 7800},
                {color: yellow, rpm: 7950},
                {color: red, rpm: 8050},
                {color: red, rpm: 8150},
            ],
            lightStyle: {
                flash: [{rpm: 8300, style: 'flash', color: [blue, off]}],
                pit: {style: '222222222222', color: ['color', 'color']}
            },
            brand: 'Porsche'},
        'ferrari_488_challenge_evo':
            {engine: 'MR',
            carClass: 'CUP',
            ECU: {ABS: 4, TC: 4, TC2: 4, MAP: 12}, // Not sure if it actually has maps.
            bias: [51, 51],
            tachDelims: [[7300, tgreen], [7600, tred], [7800, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 7300},
                {color: green, rpm: 7400},
                {color: green, rpm: 7500},
                {color: red, rpm: 7600},
            ],
            lightStyle: {
                flash: [{rpm: 7600, style: 'flash', color: ['color', off]}],
                pit: {style: '1111', color: ['color', 'color']}
            },
            brand: 'Ferrari'},
        'bmw_m2_cs_racing':
            {engine: 'FR',
            carClass: 'CUP',
            ECU: {ABS: 2, TC: 4, TC2: false, MAP: false},
            bias: [56, 56],
            tachDelims: [[6400, tgreen], [6800, tyellow], [7200, tred], [7201, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6400},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: ['color', off]}],
                pit: {style: '1000000000', color: [blue, blue]}
            },
            brand: 'BMW'},

    // GT4
        // Front
        'amr_v8_vantage_gt4':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: 10, TC: 11, TC2: false, MAP: false},
            bias: [45, 65],
            tachDelims: [[5000, tgreen], [5400, tyellow], [6200, tred], [6600, blue], [6900, 'style']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5000},
                {color: green, rpm: 5200},
                blankLight,
                {color: yellow, rpm: 5400},
                {color: yellow, rpm: 5600},
                {color: yellow, rpm: 5800},
                {color: yellow, rpm: 6000},
                {color: red, rpm: 6200},
                {color: red, rpm: 6400},
                blankLight,
                {color: lightBlue, rpm: 6600},
                {color: lightBlue, rpm: 6800},
            ],
            lightStyle: {
                flash: [{rpm: 6900, style: 'flash', color: ['color', off]}],
                pit: {style: '1000000001', color: [green, off]}
            },
            brand: 'Aston Martin'},
        'bmw_m4_gt4':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: 2, TC: 4, TC2: false, MAP: false},
            bias: [49, 58.9],
            tachDelims: [[6400, tgreen], [6800, tyellow], [7200, tred], [7201, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6400},
                {color: green, rpm: 6600},
                {color: yellow, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: red, rpm: 7200},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'flash', color: ['color', off]}],
                pit: {style: '1000000000', color: ['yellow', 'yellow']}
            },
            brand: 'BMW'},
        'chevrolet_camaro_gt4r':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: 5, TC: 5, TC2: false, MAP: false},
            bias: [47, 57],
            tachDelims: [[6000, tgreen], [7000, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 6000},
                {color: green, rpm: 6200},
                {color: green, rpm: 6400},
                {color: green, rpm: 6600},
                {color: green, rpm: 6800},
            ],
            lightStyle: {
                flash: [{rpm: 7000, style: 'solid', color: [red, red]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'Chevrolet'},
        'ginetta_g55_gt4':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: 12, TC: 9, TC2: false, MAP: false},
            bias: [46, 67],
            tachDelims: [[6300, tgreen], [6700, tyellow], [6900, tred], [6901, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: off, rpm: 0},
                {color: green, rpm: 6300},
                {color: green, rpm: 6500},
                {color: yellow, rpm: 6700},
                {color: red, rpm: 6900},
            ],
            lightStyle: {
                flash: [{rpm: 6900, style: 'solid', color: [red, red]}],
                pit: {style: '1000000001', color: [blue, blue]}
            },
            brand: 'Ginetta'},
        'maserati_mc_gt4':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: false, TC: false, TC2: false, MAP: false},
            bias: [49, 60],
            tachDelims: [[5700, tgreen], [6500, tred], [6800, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5700},
                {color: green, rpm: 5900},
                {color: green, rpm: 6100},
                {color: green, rpm: 6300},
                {color: red, rpm: 6500},
                {color: red, rpm: 6700},
            ],
            lightStyle: {
                flash: [{rpm: 6800, style: 'flash', color: ['color', off]}],
                pit: {style: '1000000001', color: [blue, off]}
            },
            brand: 'Maserati'},
        'mercedes_amg_gt4':
            {engine: 'FR',
            carClass: 'GT4',
            ECU: {ABS: 10, TC: 11, TC2: false, MAP: 3},
            bias: [51, 60.9],
            tachDelims: [[5800, tgreen], [6200, tyellow], [6500, tred], [6600, 'flash'], [6700, 'flash2']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 5800},
                {color: green, rpm: 6000},
                blankLight,
                {color: yellow, rpm: 6200},
                {color: yellow, rpm: 6400},
                {color: red, rpm: 6500},
            ],
            lightStyle: {
                flash: [{rpm: 6600, style: 'flash', color: ['color', off]},
                        {rpm: 6700, style: 'flash', color: [red, off]}],
                pit: {style: 'mercgt4', color: [green, off, red, blue, red, blue, red, blue, off, green]}
            },
            brand: 'Mercedes-Benz'},
        // Mid
        'alpine_a110_gt4':
            {engine: 'MR',
            carClass: 'GT4',
            ECU: {ABS: 10, TC: 5, TC2: false, MAP: false},
            bias: [45, 70],
            tachDelims: [[5500, tgreen], [5900, tyellow], [6100, tred], [6400, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: green, rpm: 5500},
                {color: green, rpm: 5700},
                {color: yellow, rpm: 5900},
                {color: red, rpm: 6100},
                {color: red, rpm: 6300},
            ],
            lightStyle: {
                flash: [{rpm: 6400, style: 'flash', color: ['color', off]}],
                pit: {style: '1000000001', color: [green, off]}
            },
            brand: 'Alpine'},
        'audi_r8_gt4':
            {engine: 'MR',
            carClass: 'GT4',
            ECU: {ABS: 1, TC: 3, TC2: false, MAP: false},
            bias: [50, 70],
            tachDelims: [[6400, tgreen], [7200, tyellow], [8000, tred], [8500, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6400},
                {color: green, rpm: 6700},
                {color: green, rpm: 7000},
                {color: yellow, rpm: 7200},
                {color: yellow, rpm: 7400},
                {color: yellow, rpm: 7600},
                {color: yellow, rpm: 7800},
                {color: red, rpm: 8000},
                {color: red, rpm: 8200},
                {color: red, rpm: 8400},
            ],
            lightStyle: {
                flash: [{rpm: 8500, style: 'flash', color: ['color', off]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'Audi'},
        'ktm_xbow_gt4':
            {engine: 'MR',
            carClass: 'GT4',
            ECU: {ABS: 4, TC: 11, TC2: false, MAP: false},
            bias: [44, 65],
            tachDelims: [[5700, tgreen], [6100, tyellow], [6300, tred], [6300, 'flash']],
            lightDirection: 'center',
            lights: [
                {color: off, rpm: 0},
                {color: green, rpm: 5700},
                {color: green, rpm: 5900},
                {color: yellow, rpm: 6100},
                {color: red, rpm: 6300},
            ],
            lightStyle: {
                flash: [{rpm: 6300, style: 'solid', color: [red, red]}],
                pit: {style: '1111111111', color: [green, off]}
            },
            brand: 'KTM'},
        'mclaren_570s_gt4':
            {engine: 'MR',
            carClass: 'GT4',
            ECU: {ABS: 3, TC: 3, TC2: false, MAP: false},
            bias: [60, 60],
            tachDelims: [[6200, tgreen], [6700, tred], [7200, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6200},
                {color: green, rpm: 6200},
                {color: green, rpm: 6200},
                {color: green, rpm: 6200},
                {color: red, rpm: 6700},
                {color: red, rpm: 6700},
                {color: red, rpm: 6700},
                {color: red, rpm: 6700},
                {color: lightBlue, rpm: 7200},
                {color: lightBlue, rpm: 7200},
                {color: lightBlue, rpm: 7200},
                {color: lightBlue, rpm: 7200},
            ],
            lightStyle: {
                flash: [{rpm: 7200, style: 'none', color: ['color', 'color']}],
                pit: {style: '222222222222', color: ['color', 'color']}
            },
            brand: 'McLaren'},
        'porsche_718_cayman_gt4_mr':
            {engine: 'MR',
            carClass: 'GT4',
            ECU: {ABS: 11, TC: 11, TC2: false, MAP: false},
            bias: [45, 60],
            tachDelims: [[6600, tgreen], [7000, tyellow], [7400, tred], [7600, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 6600},
                {color: green, rpm: 6600},
                {color: green, rpm: 6800},
                {color: yellow, rpm: 7000},
                {color: yellow, rpm: 7200},
                {color: yellow, rpm: 7300},
                {color: red, rpm: 7400},
                {color: red, rpm: 7500},
            ],
            lightStyle: {
                flash: [{rpm: 7600, style: 'flash', color: ['color', off]}],
                pit: {style: '11111111', color: [blue, off]}
            },
            brand: 'Porsche'},
    }
    let r = carList[CarID]
    if (r) {
        return r
    } else {
        return {
            engine: 'MR',
            carClass: 'GT3',
            ECU: {ABS: false, TC: false, TC2: false, MAP: false},
            bias: [0, 100],
            tachDelims: [[5900, tgreen], [6500, tyellow], [6700, orange], [6900, tred], [7100, 'flash']],
            lightDirection: 'right',
            lights: [
                {color: green, rpm: 5900},
                {color: green, rpm: 6100},
                {color: green, rpm: 6300},
                {color: green, rpm: 6400},
                {color: yellow, rpm: 6500},
                {color: yellow, rpm: 6600},
                {color: orange, rpm: 6700},
                {color: orange, rpm: 6800},
                {color: red, rpm: 6900},
                {color: red, rpm: 7000},
            ],
            lightStyle: {
                flash: [{rpm: 7100, style: 'flash', color: [blue, off]}],
                pit: {style: '2222222222', color: ['color', 'color']}
            },
            brand: 'McLaren'
        }
    }
}
function ACCTyrePressureDiff(tyre) {
    let pressure = $prop('TyrePressure' + tyre);
    let tires = $prop('DataCorePlugin.GameRawData.StaticInfo.dryTyresName');
    let compound = $prop('GameRawData.Graphics.TyreCompound');
    let opt = 0;
    switch (compound) {
        case 'dry_compound':
            switch (tires) {
                case 'DHA':
                case 'DH-A 305/660/315/680': // Lambo ST
                case 'DH-A 275-675_F_315_705_R': // Ferrari Challenge
                    opt = 27.1;
                    break;
                case 'DHE':
                    opt = 27.6;
                    break;
                case 'DHD2':
                    opt = 27.5;
                    break;
            }
            break;
        case 'wet_compound':
            switch ($prop('GameRawData.StaticInfo.wetTyresName')) {
                case 'WH':
                case 'WH trofeo cup 245': // Ferrari Challenge
                    opt = 30.5
                    break;
            }
            break;
    }
    return pressure - opt
}

function ACCTyrePressureRanges(tyre) {
    // Returns a color for the tyres in ACC
    // Blue for low, steelblue for almost there, green for okay,
    //   gold for a bit high, red for danger
    let pressure = $prop('TyrePressure' + tyre);
    let tires = $prop('DataCorePlugin.GameRawData.StaticInfo.dryTyresName');
    let compound = $prop('GameRawData.Graphics.TyreCompound');
    let range = 0.6;
    let bottom = 0;
    switch (compound) {
        case 'dry_compound':
            switch (tires) {
                case 'DHA':
                case 'DH-A 305/660/315/680': // Lambo ST
                case 'DH-A 275-675_F_315_705_R': // Ferrari Challenge
                    bottom = 26.8;
                    break;
                case 'DHE':
                    bottom = 27.3;
                    break;
                case 'DHD2':
                    bottom = 27.0;
                    range = 1.0;
                    break;
            }
            break;
        case 'wet_compound':
            switch ($prop('GameRawData.StaticInfo.wetTyresName')) {
                case 'WH':
                case 'WH trofeo cup 245': // Ferrari Challenge
                    bottom = 30.0;
                    range = 1.0;
                    break;
            }
            break;
    }
    if (pressure < (bottom - range)) {
        return 'blue';
    }
    else if (pressure < bottom) {
        return 'SteelBlue';
    }
    else if (pressure > (bottom + range * 2)) {
        return 'red';
    }
    else if (pressure > (bottom + range)) {
        return 'Goldenrod';
    }
    else {
        return 'green';
    }
}

function ACCTyreTempRanges(tyre) {
    let tp = $prop('TyreTemperature' + tyre)

    if ($prop('GameRawData.Graphics.RainTyres')) {
        if (tp > 100)
            return 'red'
        else if (tp > 90)
            return 'goldenrod'
        else if (tp < 40)
            return 'blue'
        else if (tp < 45.4)
            return '#5ca2df'
        else
            return 'green'
    } else {
        if (tp > 105)
            return 'red'
        else if (tp > 95)
            return 'Goldenrod'
        else if (tp < 65)
            return 'blue'
        else if (tp < 76)
            return '#5ca2df'
        else
            return 'green'
    }
}

function ACCBrakeTemp(brake) {
    let brakeTemp = $prop('BrakeTemperature' + brake)
    let brakePad = ''
    if (brake.startsWith('Rear')) {
        brakePad = $prop('DataCorePlugin.GameRawData.Physics.rearBrakeCompound')
        let minTemp = 150
        let lowTemp = 366
        let highTemp = 450
        let maxTemp = 600
        let fade = 700
        let heavyFade = 750

        switch (brakePad) {
            case 0:
                break;
            case 1:
                lowTemp = 250
                highTemp = 379
                maxTemp = 600
                fade = 700
                break;
            case 2:
                minTemp = 130
                lowTemp = 250
                highTemp = 550
                maxTemp = 800
                fade = 850
                heavyFade = 900
                break;
            case 3:
                lowTemp = 280
                highTemp = 740
                maxTemp = 742
                fade = 745
                break;	
        }
    } else {
        brakePad = $prop('DataCorePlugin.GameRawData.Physics.frontBrakeCompound')
        // Temps from here https://pastebin.com/9FGd4hix
        // fade and heavy-fade are guesses.
        let minTemp = 250
        let lowTemp = 366
        let highTemp = 450
        let maxTemp = 500
        let fade = 700
        let heavyFade = 750

        switch (brakePad) {
            case 0:
                break;
            case 1:
                lowTemp = 364
                highTemp = 425
                maxTemp = 600
                fade = 700
                break;
            case 2:
                minTemp = 200
                lowTemp = 250
                highTemp = 550
                maxTemp = 800
                fade = 850
                heavyFade = 900
                break;
            case 3:
                minTemp = 200
                lowTemp = 280
                highTemp = 740
                maxTemp = 742
                fade = 745
                break;	
        }
    }
    if (lowTemp <= brakeTemp && brakeTemp <= highTemp) {
        return 'green'
    } else if (minTemp <= brakeTemp && brakeTemp <= lowTemp) {
        return 'MediumSeaGreen'
    } else if (highTemp <= brakeTemp && brakeTemp <= maxTemp) {
        return 'gold'
    } else if (maxTemp <= brakeTemp && brakeTemp <= fade) {
        return 'goldenrod'
    } else if (fade <= brakeTemp && brakeTemp <= heavyFade) {
        return 'red'
    } else if (heavyFade <= brakeTemp) {
        return 'crimson'
    } else {
        return 'blue'
    }
}

function ACCECUMaps(carID, ecumap) {
    switch (carID) {
        case 'amr_v12_vantage_gt3':
        case 'amr_v8_vantage_gt3':
        case 'audi_r8_lms':
        case 'audi_r8_lms_evo':
        case 'audi_r8_lms_evo_ii':
        case 'bentley_continental_gt3_2016':
        case 'bentley_continental_gt3_2018':
        case 'lamborghini_huracan_gt3':
        case 'lamborghini_huracan_gt3_evo':
            switch (ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, Norm, Agg'
                case 3: return 'D:Fast, Norm, Prog'
                case 4: return 'D:Slow, Low, Prog'
                case 5: return 'W:Fast, Norm, Prog'
                case 6: return 'W:Slow, Low, Prog'
                case 7: return 'W:Slow, Low, VProg'
                case 8: return 'PACE CAR'
            }
        case 'bmw_m6_gt3':
        case 'bmw_m4_gt3':
            switch(ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, Norm, Lin'
                case 3: return 'D:Fast, Norm, Grad'
                case 4: return 'ENGINEER ONLY'
                case 5: return 'PACE CAR'
                case 6: return 'W:Fast, Norm, Prog'
                case 7: return 'W:Slow, Low, Prog'
                case 8: return 'W:Slow, Low, VProg'
            }
        case 'ferrari_488_gt3':
            switch(ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, High, Agg'
                case 3: return 'D:Norm, Norm, Agg'
                case 4: return 'D:Slow, Low, MAgg'
                case 5: return 'W:Fast, High, Grad'
                case 6: return 'W:Norm, Norm, Grad'
                case 6: return 'W:Norm, Norm, Grad'
                case 7: return 'W:Slow, Low, Grad'
                case 9: return 'PACE CAR 1'
                case 10: return 'PACE CAR 2'
                case 11: return 'PACE CAR 3'
                case 12: return 'PACE CAR 4'
            }
        case 'ferrari_488_gt3_evo':
        case 'ferrari_488_challenge_evo': // Assuming this is the same...
            switch(ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, High, Agg'
                case 3: return 'D:Norm, Norm, Agg'
                case 4: return 'D:Norm, Low, MAgg'
                case 5: return 'D:Norm, VLow, Lin'
                case 6: return 'W:Fast, High, Grad'
                case 7: return 'W:Norm, Norm, Grad'
                case 8: return 'W:Slow, Low, Grad'
                case 9: return 'PACE CAR 1'
                case 10: return 'PACE CAR 2'
                case 11: return 'PACE CAR 3'
                case 12: return 'PACE CAR 4'
            }
        case 'honda_nsx_gt3':
        case 'honda_nsx_gt3_evo':
            switch (ecumap) {
                case 1: return 'D:Full, Prog'
                case 2: return 'D:Full, Lin'
                case 3: return 'D:Full, Agg'
                case 4: return 'D:Full, VAgg'
                case 5: return 'W:Full, Prog'
                case 6: return 'W:Full, VProg'
                case 7: return 'W:Slow, Low, VProg'
                case 8: return 'PACE CAR'
            }
        case 'lamborghini_gallardo_rex':
            switch(ecumap) {
                case 1: return 'Fastest, Highest'
                case 2: return 'Fast, Norm, Agg'
                case 3: return 'Fast, Norm, Prog'
                case 4: return 'Slow, Low, Prog'
            }
        case 'jaguar_g3':
            switch(ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, Norm, Lin'
                case 3: return 'D:Slow, Low, Lin'
                case 4: return 'W:Fast, High, Grad'
                case 5: return 'W:Norm, Norm, Grad'
                case 6: return 'W:Slow, Low, Grad'
            }
        case 'nissan_gt_r_gt3_2017':
        case 'nissan_gt_r_gt3_2018':
            switch(ecumap) {
                case 1: return 'D:Fastest, Highest'
                case 2: return 'D:Fast, Norm'
                case 3: return 'D:Norm, Low'
                case 4: return 'D:Slow, Low'
            }
        case 'lexus_rc_f_gt3':
            switch (ecumap) {
                case 1: return 'D:Linear'
                case 2: return 'D:Aggressive'
                case 3: return 'D:Progressive'
                case 4: return 'W:Fuel Save'
                case 5: return 'PACE CAR'
            }
        case 'mclaren_650s_gt3':
            switch(ecumap) {
                case 1: return 'D:Fast, High'
                case 2: return 'D:Norm, Norm'
                case 3: return 'D:Norm, Low'
                case 4: return 'D:Slow, VLow'
                case 5: return 'W:Fast, High'
                case 6: return 'W:Norm, Norm'
                case 7: return 'W:Norm, Low'
                case 8: return 'W:Slow, VLow'
                case 9: return 'PACE CAR'
            }
        case 'mclaren_720s_gt3':
            switch(ecumap) {
                case 1: return 'D:Quali'
                case 2: return 'D:Fast, High'
                case 3: return 'D:Fast, High, Prog'
                case 4: return 'D:Norm, Norm'
                case 5: return 'D:Low, Low'
                case 6: return 'D:Lowest, Lowest'
                case 7: return 'COOL DOWN MAP'
                case 8: return 'Damp:Quali'
                case 9: return 'Damp:Race'
                case 10: return 'W:Quali'
                case 11: return 'W:Fast, Norm'
                case 12: return 'W:Fast, Norm'
            }
        case 'mercedes_amg_gt3':
        case 'mercedes_amg_gt3_evo':
        case 'mercedes_amg_gt4':
            switch(ecumap) {
                case 1: return 'Fastest, Highest'
                case 2: return 'Normal, Normal'
                case 3: return 'Slow, Low'
            }
        case 'porsche_991_gt3_r':
        case 'porsche_991ii_gt3_r':
            switch(ecumap) {
                case 1: return 'Norm, Norm, LProg'
                case 2: return 'Norm, Norm, Prog'
                case 3: return 'Norm, Norm, Agg'
                case 4: return 'Norm, Norm, Linear'
                case 5: return 'Qual, High, LProg'
                case 6: return 'Qual, High, Prog'
                case 7: return 'Qual, High, Agg'
                case 8: return 'Qual, High, Linear'
                case 9: return 'Slow, Low, Prog'
                case 10: return 'PACE CAR'
            }
        default:
            return ''
    }
}

function ACCFuel() {
    // Lets us see fuel even when we are in the pits, etc.
    let fuel = $prop('Fuel')
    if (fuel) {
        return fuel
    } else {
        let fuelPerLap = $prop('GameRawData.Graphics.FuelXLap')
        let lapsRemaining = $prop('GameRawData.Graphics.fuelEstimatedLaps')
        return fuelPerLap * lapsRemaining
    }
}

function ACCFuelDelta() {
    let timeRemaining = timespantoseconds($prop('SessionTimeLeft'))
    let fuelRemaining = ACCFuel()
    let fuelPerLap = $prop('GameRawData.Graphics.FuelXLap')
    let estLapTime = $prop('GameRawData.Graphics.iEstimatedLapTime') / 1000
    if (estLapTime >= 2047483) {
        // Only use the Persistant Tracker plugin if we have to
        estLapTime = timespantoseconds($prop('PersistantTrackerPlugin.EstimatedLapTime'))
    }
    // For when we're in pits after session start, or on an in/out lap
    if(estLapTime > (1.2 * timespantoseconds($prop('PersistantTrackerPlugin.AllTimeBest')))) {
        estLapTime = timespantoseconds($prop('PersistantTrackerPlugin.AllTimeBest'))
    }

    let lapsRemaining = timeRemaining / estLapTime

    let fuelToFinish = lapsRemaining * fuelPerLap
    let endFuel = fuelRemaining - fuelToFinish

    if (endFuel > 9999) { return '--.-' }
    else if (endFuel < -9999) { return '--.-' }
    else return endFuel.toFixed(2)
}

function ACCFuelTime() {
    let lapsRemaining = $prop('GameRawData.Graphics.fuelEstimatedLaps')
    let estLapTime = $prop('GameRawData.Graphics.iEstimatedLapTime') / 1000
    if (estLapTime >= 2047483) {
        // Only use the Persistant Tracker plugin if we have to
        estLapTime = timespantoseconds($prop('PersistantTrackerPlugin.EstimatedLapTime'))
    } else if(estLapTime > (1.2 * timespantoseconds($prop('PersistantTrackerPlugin.AllTimeBest')))) {
        estLapTime = timespantoseconds($prop('PersistantTrackerPlugin.AllTimeBest'))
    }

    return secondstotimespan(lapsRemaining * estLapTime)
}

function ACCPitWindow() {
    // switching these for sanity
    let pitEnd = $prop('GameRawData.StaticInfo.PitWindowStart') / 1000
    let pitStart = $prop('GameRawData.StaticInfo.PitWindowEnd') / 1000

    let sessionRemaining = timespantoseconds($prop('SessionTimeLeft'))

    if (sessionRemaining > pitEnd) {
        return secondstotimespan(sessionRemaining - pitEnd)
    }

    return (pitEnd - sessionRemaining)
}

function ACCBBIndicator() {
    try {
        let bbRange = ACCCars($prop('CarId')).bias
        let bb = $prop('BrakeBias')

        let bbMax = bbRange[1]
        let bbMin = bbRange[0]

        let range = bbMax - bbMin

        let width = 77

        if (range == 0) {
            return (width / 100) * (bb)
        } else {
            return (width / range) * (bb - bbMin)
        }
    } catch (error) {
        return 0
    }
}

function ACCDriverShortName(name) {
    // pos is a string, ie Ahead_04 or Behind_00
    let initial = name[0]
    let lastName = name.split(' ')[1]
    return initial + '. ' + lastName
}

function ACCDriverName(pos) {
    // pos is a string, ie Ahead_04 or Behind_00
    let driver =  $prop('PersistantTrackerPlugin.Driver' + pos + '_Name')
    return ACCDriverShortName(driver)
}

function ACCSwoopAccess(item) {
    return $prop('SimHubSwoopPlugin.' + item)
}

function ACCSwoopDict(pos) {
    let swoopPos = ''
    let posDriver = $prop('PersistantTrackerPlugin.Driver' + pos + '_CarNumber')
    for (let i = 1; i <= 30; i++) {
        swoopPos = 'SWLeaderBoard.Position' + i
        if (ACCSwoopAccess(swoopPos + '.RaceNumber') == posDriver) {
            break
        }
    }

    return {
        RaceNumber: ACCSwoopAccess(swoopPos + '.RaceNumber'),
        BestLapTime: ACCSwoopAccess(swoopPos + '.BestlapTime'),
        // Driver info
        Driver: ACCSwoopAccess(swoopPos + '.DriverFullName'),
        DriverShort: ACCSwoopAccess(swoopPos + '.DriverShortName'),
        DriverCategory: ACCSwoopAccess(swoopPos + '.DriverCategory'),
        DriverCupCategory: ACCSwoopAccess(swoopPos + '.CupCategoryString'),
        RaceAppTag: ACCSwoopAccess(swoopPos + '.RaceAppTag'),
        CarModel: ACCSwoopAccess(swoopPos + '.CarModel'),
        CarClass: ACCSwoopAccess(swoopPos + '.Serie'),
        CarLocation: ACCSwoopAccess(swoopPos + '.CarLocation'),
        CurrentLap: ACCSwoopAccess(swoopPos + '.CurrentLapNumber'),
        Gap: $prop('PersistantTrackerPlugin.Driver' + pos + '_Gap')
    }
}

function ACCshiftLight(lightIndex) {
    // Default of 12 lights in the dash, splits nicely into all different types
    // 8 and 10 use the middle lights
    // 6 will use pairs of lights
    // 4 will use trios of lights
    let off = 'DimGray'
    let blankLight = {color: off, rpm: 0}

    let car = ACCCars($prop('CarId'))
    let lightNumber = car.lights.length
    let lights = car.lights
    let light = {}
    let rpm = $prop('Rpms')
    let flash = car.lightStyle.flash[0]
    if (car.lightDirection == 'center') {
        lightNumber = lightNumber * 2
        let rlights = lights.slice().reverse()
        lights = lights.concat(rlights)
    }
    switch(lightNumber) {
        case 10:
            // pad the edges with blank lights
            lights.unshift(blankLight)
            lights = lights.concat(blankLight)
            light = lights[lightIndex]
            break;
        case 8:
            // pad the edges with blank lights
            lights.unshift(blankLight, blankLight)
            lights = lights.concat(blankLight, blankLight)
            light = lights[lightIndex]
            break;
        case 6:
            rawInd = lightIndex / 2
            ind = rawInd - (rawInd % 1)
            light = lights[ind]
            break;
        case 4:
            rawInd = lightIndex / 3
            ind = rawInd - (rawInd % 1)
            light = lights[ind]
            break;
        default:
            light = lights[lightIndex]
            break;
    }

    if (rpm >= light.rpm) {
        if (rpm >= flash.rpm) {
            switch (flash.style) {
                case 'flash':
                    let fcolor = flash.color
                    if (fcolor[0] == 'color') { fcolor[0] = light.color }
                    if (fcolor[1] == 'color') { fcolor[1] = light.color }
                    return blink('ACCShiftLight' + lightIndex, 150, true) ? fcolor[0] : fcolor[1]
                case 'solid':
                    return flash.color[0]
                case 'none':
                    break;
            }
        }
        return light.color
    } else {
        return off
    }
}

function ACCTachBarColors() {
    let car = ACCCars($prop('CarId'))
    let delims =  car.tachDelims
    delims.reverse()
    let currRpm = $prop('Rpms')

    let off = 'dimGray'
    let blue = 'dodgerblue'

    try {
        for (let i = 0; i < delims.length; i++) {
            const crpm = delims[i];
            if (currRpm >= crpm[0]) {
                if (crpm[1] == 'flash') {
                    if (car.lightStyle.flash[0].color[0] == 'color') {
                        return 'Blue'
                    } else { return car.lightStyle.flash[0].color[0] }

                }
                return crpm[1]
            }
        }
        return blue
    } catch (error) {
        return off
    }
}

function ACCRedlineAtFlash() {
    let car = ACCCars($prop('CarId'))
    let flashRPMPercent = car.lightStyle.flash[0].rpm / $prop('CarSettings_MaxRPM')
    let width = 640

    return (flashRPMPercent * width)
}

function ACCSDelta(sIndex) {
    let s = [null, 3, 1, 2][sIndex]
    let lastSecTime = timespantoseconds($prop('LastSectorTime'))
    let bestSecTime = timespantoseconds($prop('Sector' + s + 'BestTime'))
    let tdiff = format(lastSecTime - bestSecTime, '0.000', true)
    if (! lastSecTime > 0 && ! bestSecTime > 0) {
        return null
    } else {
        return '|' + format(lastSecTime, '0.000') + '|' + tdiff
    }
}

function ACCSDeltaRaw(sIndex) {
    return format(
        timespantoseconds($prop('LastSectorTime') - $prop('Sector' + [null, 3, 1, 2][sIndex] + 'BestTime')),
        '0.000',
        true
    )
}

/*
* I need to do something about the red/green colors for the delta.
*/
switch ($prop('GameRawData.Graphics.Flag')) {
    case 1:
    case 3:
    case 5:
    case 7:
        return 'white'
    case 2:
    case 4:
    case 6: // Penalty (black/white triangles), no text
    case 8: // Warning/mechanical flag, no text
        return 'black'
}
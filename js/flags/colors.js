switch ($prop('GameRawData.Graphics.Flag')) {
    case 1: return 'blue';
    case 2: return 'gold';
    case 3: return 'black';
    case 4: return 'white';
    case 5: return 'Transparent'; // Checkered Flag
    case 6: return 'Transparent'; // Penalty (black/white triangles)
    case 7: return 'green';
    case 8: return 'Transparent'; // Warning/mechanical flag
}
if ($prop('DataCorePlugin.GameRawData.Graphics.globalYellow') ||
    $prop('DataCorePlugin.GameRawData.Graphics.FlagsDetails.IsACC_YELLOW_FLAG'))
    return 'gold'
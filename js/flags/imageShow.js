switch ($prop('GameRawData.Graphics.Flag')) {
    case 5: return 1; // Checkered Flag
    case 6: return 1; // Penalty (black/white triangles)
    case 8: return 1; // Warning/mechanical flag
}
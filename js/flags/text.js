if ($prop('GameRawData.Graphics.FlagsDetails.IsACC_YELLOW_FLAG') ||
    $prop('DataCorePlugin.GameRawData.Graphics.globalYellow')) {
    return [($prop('GameRawData.Graphics.globalYellow1') ? 'S1': '  '),
            ($prop('GameRawData.Graphics.globalYellow2') ? 'S2': '  '),
            ($prop('GameRawData.Graphics.globalYellow3') ? 'S3': '  ')].join('  ')
}
else {
    switch ($prop('GameRawData.Graphics.Flag')) {
        case 1: return 'BLUE';
        case 3: return 'BLACK';
        case 4: return 'WHITE';
        case 5: return ''; // Checkered Flag
        case 6: return 'PEN'; // Penalty (black/white triangles)
        case 7: return 'GREEN';
        case 8: return ''; // Warning/mechanical flag
    }
}
switch ($prop('GameRawData.Graphics.Flag')) {
    case 5: return 'FlagCheckered'; // Checkered Flag
    case 6: return 'FlagPenalty'; // Penalty (black/white triangles)
    case 8: return 'FlagWarning'; // Warning/mechanical flag
}
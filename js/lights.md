# Vehicle Shift Lights

## GT3

### AMR V12

* 8 lights, left to right light, circular (old)
* 2 green 2 yellow, 2 red, 2 blue
  * 1 green 6100
  * 2 green 6300
  * 1 yellow 6500
  * 2 yellow 6700
  * 1 red 6900
  * 2 red 7100
  * 1 blue 7300
  * 2 blue 7500
* flash on 7600
* on-off flash
* pit: all lights solid on

### AMR V8

* 8 lights, left to right, circular (old)
* 1 green, 4 red, 3 blue
  * 1 green 6200
  * 1 red 6400
  * 2, 3, 4 red 6600
  * 1, 2, blue 6800
  * 3 blue 7000
* flash on 7000
* on-off flash
* pit: on-off flash all lights

### Audi R8 LMS GT3, normal and evo and evo II, Lambo, Lambo Evo

* 10 lights, left to right, separated 2-6-2
* 4 green, 2 yellow, 4 red
  * 1 green 5700
  * 2 green 6000
  * 3 green 6300
  * 4 green 6600
  * 1 yellow 6800
  * 2 yellow 7000
  * 1 red 7200
  * 2 red 7400
  * 3 red 7600
  * 4 red 7800
* flash at 8000
* VERY fast color/blue flash
* pit:
  * audi, evo, evo II: 1, 3, 5, 6, 8, 10 flash, on-off blue, no shift
  * lambo: left 5 on-off blue, no shift
  * lambo evo: middle 6 on-off blue, no shift lights

### Bentley 2015

* 8 lights, left right
* 4 green, 2 blue, 2 red
  * 1 green 5900
  * 2 green 6200
  * 3 green 6500
  * 4 green 6700
  * 1, 2, blue 6900
  * 1, 2, red 7100
* flash at 7100
* on-off flash
* pit: blue-off flash

### Bentley 2018

* 8 lights, to center
* 1 green, 2 yellow, 1 red
  * 1 green 6300
  * 1 yellow 6500
  * 2 yellow 6700
  * 1 red 6900
* flash at 6900
* on-off flash
* pit: blue-off flash

### BMW M4 GT3

* 10 lights, 2-6-2, new DDU, to center
* 2 green, 2 yellow, 2 red
  * 1 green 5600
  * 2 green 6000
  * 1 yellow 6200
  * 2 yellow 6400
  * 1 red 6600
* flash at 6600
* all red flash
* pit: Pink-off flash on outer lights

### BMW M6 GT3

* 8 lights, left to right, circular
* 4 green, 2 yellow, 2 red
  * 1 green 4700
  * 2 green 5000
  * 3 green 5300
  * 4 green 5500
  * 1 yellow 5700
  * 2 yellow 5900
  * 1 red 6100
  * 2 red 6250
* flash 6350
* on-off flash
* pit: no shift, blue light on left edge

### Jag

* 10 lights, left to right, ssquare
* 4 green, 2 yellow, 2 orange, 2 red
  * 1, 2 green 6800
  * 3, 4 green 7100
  * 1, 2 yellow 7400
  * 1, 2 orange 7700
  * 1, 2 red 8000
* flash at 8100
* on-off flash
* pit: nothing different.

### Ferrari 488, original and EVO

* 6 lights, left to right, circular
* 4 green, 1 yellow, 1 red
  * 1 green 6100
  * 2 green 6300
  * 3 green 6500
  * 4 green 6700
  * 1 yellow 6900
  * 1 red 7000
* flash at 7250
* red on-off flash
* pit: flash on-off blue

### Honda NSX and evo

* 10 lights, left to right, modern
* 4 green, 2 yellow, 2 orange, 2 red
  * 1,2 green 6200
  * 3,4 green 6500
  * 1,2 yellow 6700
  * 1,2 orange 6900
  * 1,2 red 7100
* flash at 7200
* on-off flash
* pit: nothing different.

### Lexus RC F GT3

* 8 lights, left right, circular
* 3 green, 1 yellow, 2 red, 2 blue
  * 1 green 6000
  * 2 green 6200
  * 3 green 6400
  * 1 yellow 6600
  * 1 red 6800
  * 2 red 6900
  * 1 blue 7000
  * 2 blue 7100
* flash at 7250
* on-off flash
* pit: all blue-off flash

### Mclaren 650S

* 12 lights, in-dash, circular
* 4 green, 4 red, 4 blue
  * 1 green 6100
  * 2 green 6250
  * 3 green 6400
  * 4 green 6500
  * 1 red 6600
  * 2 red 6700
  * 3 red 6800
  * 4 red 6900
  * 1, 2, 3, 4 blue 7000
* no flash
* pits: only 1st green shift light

### Mclaren 720S

* 10 lights, 2-6-2, new DDU
* 4 green, 2 yellow, 2 orange 2 red
  * 1 green 5900
  * 2 green 6100
  * 3 green 6300
  * 4 green 6400
  * 1 yellow 6500
  * 2 yellow 6600
  * 1 orange 6700
  * 2 orange 6800
  * 1 red 6900
  * 2 red 7000
* flash 7100
* blue on-off flash
* pit: no shift lights, white lights on sides

### Merc AMG, evo

* 10 lights, to center, 2-6-2, old circular
* 2 green, 2 yellow, 1 red
  * 1 green 6000
  * 2 green 6300
  * 1 yellow 6600
  * 2 yellow 6800
  * 1 red 7000
* flash at 7100
* red on-off flash
* pit:
  * outer flash alternating green-off,
  * inner flash alternating blue-red
  * christmas tree

### Nissan GTR 2015

* 10 lights, to center, old circular
* all blue
  * 2 blue 6500
  * 3 blue 6800
  * 4 blue 7000
  * 5 blue/red center/flash 7200
* flash at 7200
* red on-off flash
* pit:
  * flash outer blue-off lights, no rpm lights
  * 2018: flash outer blue-yellow lights, no rpm lights

### Porsche 991 GT3 R

* 8 lights, left to right, circular
* 3 green, 3 yellow, 2 red
  * 1 green 7400
  * 2 green 7600
  * 3 green 7800
  * 1 yellow 8000
  * 2 yellow 8200
  * 3 yellow 8400
  * 1 red 8600
  * 2 red 8800
* flash at 9000
* on-off flash
* pit: blue-off flash

### Porsche 991.2 GT3 R

* 10 lights, to center, circular
* 2 green, 2 yellow, 1 red
  * 1 green 7400
  * 2 green 7700
  * 1 yellow 8000
  * 2 yellow 8300
  * 1 red 8500
* flash at 8700
* blue on-off flash
* pit: blue-off flash

### Reiter

* 8 lights, left to right
* 2 green, 2 yellow, 4 red
  * 1,2 green 7300
  * 1,2 yellow 7600
  * 1,2 red 7900
  * 3,4 red 8200
* no flash
* pit: no shift lights, random blue light on right

## GT4

### Alpine

* 10 lights, to center, round
* 2 green, 1 yellow, 2 red
  * 1 green 5500
  * 2 green 5700
  * 1 yellow 5900
  * 1 red 6100
  * 2 red 6300
* flash at 6400
* on-off flash
* pit: small clusters outside of shift light on-off blue

### AMR V8 GT4

* 10 shift lights, 2-6-2, left to right
* 2 green, 4 yellow, 2 red, 2 blue
  * 1 green 5000
  * 2 green 5200
  * 1 yellow 5400
  * 2 yellow 5600
  * 3 yellow 5800
  * 4 yellow 6000
  * 1 red 6200
  * 2 red 6400
  * 1 blue 6600
  * 2 blue 6800
* flash at 6900
* on-off flash
* pit: no shift lights, fast green flashes on edges

### Audi R8 LMS GT4

* 10 shift lights, left to right, square
* 3 green, 4 yellow, 3 red
  * 1 green 6400
  * 2 green 6700
  * 3 green 7000
  * 1 yellow 7200
  * 2 yellow 7400
  * 3 yellow 7600
  * 4 yellow 7800
  * 1 red 8000
  * 2 red 8200
  * 3 red 8400
* flash at 8500
* on-off flash
* pit: no changes

### BMW M4 GT4

* 10 lights, to center, square
* 2 green, 2 yellow, 1 red
  * 1 green 6400
  * 2 green 6600
  * 1 yellow 6800
  * 2 yellow 7000
  * 1 red 7200
* flash at 7200
* on-off flash
* pit: no lights, yellow light on left edge

### Chevy

* 10 lights, to center, extra 2 lights on top left
* 5 green
  * 1 green 6000
  * 2 green 6200
  * 3 green 6400
  * 4 green 6600
  * 6 green 6800
* flash at 7000
* red-solid, also light up extra lights
* pit: no changes

### Ginetta

* 10 lights, to center, outer lights unused
* 2 green, 1 yellow, 1 red
  * 1 green 6300
  * 2 green 6500
  * 1 yellow 6700
  * 1 red 6900
* flash at 6900
* solid red
* pit: no shift, outer lights solid blue

### KTM

* 10 lights, to center, outmost not used
* 2 green, 1 yellow, 1 red
  * 1 green 5700
  * 2 green 5900
  * 1 yellow 6100
  * 1 red 6300
* flash at 6300
* solid red
* pit: all flash green-off

### Maserati

* 8 lights, left to right, outermost not used
* 4 green, 2 red
  * 1 green 5700
  * 2 green 5900
  * 3 green 6100
  * 4 green 6300
  * 1 red 6500
  * 2 red 6700
* flash at 6800
* on-off flash
* pit: outermost flash blue-off

### Mclaren 570S GT4

* 12 lights, left to right, digital circles
* 4 green, 4 red, 4 blue
  * 1,2,3,4 green 6200
  * 1,2,3,4 red 6700
  * 1,2,3,4 blue 7200
* no flash
* pit: no changes

### Merc AMG GT4

* 10 lights, to center, 2-6-2, circular
* 2 green, 2 yellow, 1 red
  * 1 green 5800
  * 2 green 6000
  * 1 yellow 6200
  * 2 yellow 6400
  * 1 red 6500
* flash at 6600
* on-off flash
* secondary flash at 6700
* red-off flash
* pit: solid green, off, red, blue, red, blue, red, blue, off, green pattern.

### Porsche 718

* 8 lights, left to right, circular
* 3 green, 3 yellow, 2 red
  * 1,2 green 6600
  * 3 green 6800
  * 1 yellow 7000
  * 2 yellow 7200
  * 3 yellow 7300
  * 1 red 7400
  * 2 red 7500
* flash at 7600
* on-off flash
* pit: blue-off flash

## Cup Cars

### Porsche 991.2 Cup

* 8 lights, left to right, circular
* 3 green, 3 yellow, 2 red
  * 1 green 7000
  * 2 green 7200
  * 3 green 7400
  * 1 yellow 7600
  * 2 yellow 7800
  * 3 yellow 7900
  * 1 red 8000
  * 2 red 8100
* flash at 8300
* on-off flash
* pit: all blue-off flash

### Porsche 992 Cup

* 16 lights, to center, circular
* 3 green, 3 yellow, 2 red
  * 1 green 7000
  * 2 green 7200
  * 3 green 7400
  * 1 yellow 7600
  * 2 yellow 7800
  * 3 yellow 7950
  * 1 red 8050
  * 2 red 8150
* flash at 8300
* blue-off flash
* pit: no changes

### Lambo ST

* 10 lights, to center, outermost unused
* 1 green, 1 blue, 1 yellow, 1 red
  * 1 green 6200
  * 1 blue 6800
  * 1 yellow 7100
  * 1 red 7400
* flash at 7900
* on-off flash
* pit: alternating blue-off lights

### Lambo ST EVO2

* 10 lights, to center, outermost unused
* 1 green, 1 blue, 1 yellow, 1 red
  * 1 green 7600
  * 1 blue 7800
  * 1 yellow 8000
  * 1 red 8200
* flash at 8400
* on-off flash
* pit: alternating blue-off lights

### Ferrari 488 Challenge Evo

* 4 lights, left to right, circular
* 3 green, 1 red
  * 1 green 7300
  * 2 green 7400
  * 3 green 7500
  * 1 red 7600
* flash at 7800
* on-off flash
* pit: all lights solid on

### BMW M2 CS

* 10 lights, new DDU, to center
* 2 green, 2 yellow, 2 red
  * 1 green 6400
  * 2 green 6600
  * 1 yellow 6800
  * 2 yellow 7000
  * 1 red 7200
* flash at 7200
* on-off flash
* pit: no shift lights, small light on left

# SforzaDashM

[RaceDepartment](https://www.racedepartment.com/downloads/sforzadash.47251/)  
[Gitlab](https://gitlab.com/kaictl/racing/SforzaDash)

## Description

The modernized SforzaDash, or SforzaDashM, is a complete overhaul for
performance and visuals. It's significantly smaller, almost a third the size of
the original. There has been liberal use of transparency, as well, so this
should work *even better* in VR, but also on a normal screen. Outside of racing,
and in the setup menu the entire screen will be transparent, with a small 1px
line on the bottom to help with allignment before you start driving.

Here's an overview of the current setup.

![Main](SforzaDashM/SforzaDashM.djson.png)

* Shift lights modeled after the MCL 720S' DDU10 lights
* Dynamically adjusting tachometer up to 15000 RPM
* Engine status indicator at beginning of the tach
* Wheel slip lights (oversteer, understeer, wheel locking, etc.)
* Light indicators for headlights, rain light and turn signals
* Speed, gear and numeric RPM in the center
* Built-in fuel calculator
  * Remaining fuel
  * Fuel per lap
  * Laps remaining
  * Time remaining (based on session fastest or persistent fastest if session
    fastest not set)
  * Fuel delta
* Weather and track info (forecast, track temp and grip status)
* Tyre status includes
  * Temperature colors on the outside
  * Brake temperatures in the center
  * Pressures with numbers between them
* ECU status with brake bias, engine map, TC and ABS
  * TC and ABS will flash when active
* Detailed timing and session information
  * Predicted time and gap based on current session best, otherwise based on
    persistent data best lap.
  * Delta bar +/- 2s.
  * Current time, turns red when invalid
  * Session best lap time
  * Last lap time
  * Session time
  * Real world time
  * Laps completed over total race laps
* Pop ups for brake bias, tc, tc2 and ABS
* Notification pop ups for lap times, low fuel, etc.
* Damage notification and severity for body and suspension
* Detailed damage in a sub-page for information

This is meant to replace the dashboard. It does not replace the racelogic timer
or the Lumirank flag screen.

## Installation

Simply extract the `.simhubdash` file and double-click on it. This should bring
up SimHub if you have it installed and allow you to add it to the dash studio.

### Development

This is a bit more of an involved process, but will also allow you to
auto-update your dash in simhub just by doing a `git pull` when changes happen.
To do this we need to use symlinks in Windows to point from the simhub install
directory, where all of the dashes are stored as `djson` files and wherever your
git repository is.

1. Find your simhub install directory. For this example we'll be using
`C:\Program Files\simhub`, which is probably where your install is, as well.

2. Clone this repository in a user directory, I use `~\git\SforzaDash`.

3. If you already imported this dashboard before, be sure to remove it first.

4. From an administrator powershell session, run the command below.

   The `Target` will be something like
   `C:\Users\kaictl\git\SforzaDash\SforzaDashM`. The Documentation for this
   command can be found [here][New-Item].  This is the only part that requires
   administrator privileges, and this window can be closed as soon as this is
   complete. The rest of this process does not require any administrator
   privileges. You will also want to link the javascript stuff, or the dash
   will be mostly empty.

   ```pwsh
   New-Item -ItemType SymbolicLink `
       -Path PATH_TO_SIMHUB\DashTemplates\SforzaDash `
       -Target PATH_TO_GIT_REPO\SforzaDash
   New-Item -ItemType SymbolicLink `
       -Path PATH_TO_SIMHUB\DashTemplates\SforzaDashM `
       -Target PATH_TO_GIT_REPO\SforzaDashM
   New-Item -ItemType SymbolicLink `
       -Path PATH_TO_SIMHUB\JavascriptExtensions\ACC.js `
       -Target PATH_TO_GIT_REPO\js\main.js
   ```

5. Check SimHub and make sure it shows up as `SforzaDash` and `SforzaDashM`

6. On any updates, you can simply `git pull` and either `git checkout vX.Y.Z`
   for a tagged version, or `git checkout master` to get the latest development
   version.

## Thanks

Enjoy, and let me know if there's any other stuff I'm missing!

[New-Item]: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/new-item?view=powershell-7.2#example-7--create-a-symbolic-link-to-a-file-or-folder
[carinfo]: https://www.gtplanet.net/forum/threads/acc-the-complete-car-teams-and-drivers-list-now-inc-2021-liveries-update.393917/
